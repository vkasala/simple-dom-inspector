/**
 * WAP Project:     Document Object Model (DOM) Inspector
 * Author:          Bc. Viliam Kasala, xkasal01@stud.fit.vutbr.cz
 * Description:     Implement simple document object model inspector.
 **/

/**
 * Class encapuslating DomInspector object.
 * @constructor
 */
function DomInspector() {
    this.panelId = 'dom-inspector-wrapper';

    this.panelTitle = 'Dom Inspector Panel';

    this.attirbuteTitle = 'Attribute';

    this.panelContentId = 'dom-items-panel-content';

    this.inputIDPanelID = 'dom-inspector-id-input';

    this.inputClassPanelID = 'dom-inspector-class-input';

}

/**
 * Getter method.
 * @returns {string}
 */
DomInspector.prototype.getIDPanelText = function () {
    return document.getElementById(this.inputIDPanelID).value.trim();
}

/**
 * Getter method.
 * @returns {string}
 */
DomInspector.prototype.getClassPanelText = function () {
    return document.getElementById(this.inputClassPanelID).value.trim();
}

/**
 * Method for creating element from arguments.
 * @param Element - name of an Element. (e.g. 'div')
 * @param Map - pair of name/value of attributes.
 * @returns {HTMLElement} created HTMLElement
 */
DomInspector.prototype.createElementFromArgs = function (Element, Map) {
    var element = document.createElement(Element);

    for (var attribute in Map) {
        if (attribute == 'innerHTML') {
            element.innerHTML = Map[attribute];
            continue;
        } else if (attribute == 'appendToElement') {
            Map[attribute].appendChild(element);
            continue;
        }
        element.setAttribute(attribute, Map[attribute]);
    }
    return element;

}

/**
 * Method creates HTML code for Dom Inspector Panel.
 */
DomInspector.prototype.initialize = function () {

    this.domInspectorWrapper = this.createElementFromArgs('div', {
        'id': 'dom-inspector-wrapper'
    });

    this.domInspectorWrapper.innerHTML = "<div id='dom-items-panel'>" +
        "<div id='dom-items-panel-title' class='dom-panel-title'>Dom Inspector Panel</div>" +
        "<div id='dom-items-panel-content'></div></div>" +
        "<div id='dom-attribute-panel'>" +
        "<div id='dom-attribute-panel-title' class='dom-panel-title'>Attributes</div>" +
        "<div id='dom-attribute-panel-content'>" +
        "<span class='dom-attribute-panel-content-attribute'>" +
        "<label>ID: <input id='dom-inspector-id-input' type='text' /></label>" +
        "</span>" +
        "<span class='dom-attribute-panel-content-attribute'>" +
        "<label>Class: <input id='dom-inspector-class-input' type='text' /></label>" +
        "</span></div></div>";
}


/**
 * Method traverses all nodes in DOM and creates XPath for each node.
 * @param Nodes - array of nodes.
 * @param HTMLElement - parent HTML element.
 * @param InHTMLElement - parent HTML element.
 * @param XPath - accumulated XPath.
 */
DomInspector.prototype.addNodes = function (Nodes, HTMLElement, InHTMLElement, XPath) {

    var domCounter = 0;

    for (var i = 0; i < Nodes.length; i++) {
        if (Nodes.item(i).nodeType == 1 || Nodes.item(i).nodeType == 3) {
            domCounter++;
            this.addNode(Nodes.item(i), HTMLElement, InHTMLElement, XPath + "[" + domCounter + "]");
        }
    }
}

/**
 * Method creates HTML element for each given Node and add it into HTML code of DOM panel.
 * @param Node - node which has to be added.
 * @param HTMLParentElement - HTML element, where new node has to be created.
 * @returns {HTMLElement} - return new HTML element that corresponds to given element.
 */
DomInspector.prototype.createDomInspectorElement = function (Node, HTMLParentElement) {

    var xPath = getPathTo(Node);

    // First create li element.
    var li = this.createElementFromArgs('li', {
        'class': 'dom-inspector-panel-content-li',
        'appendToElement': HTMLParentElement
    });

    this.createElementFromArgs('span', {
        'class': 'dom-inspector-panel-content-trigger dom-unpacked',
        //'onclick' : 'domInspectorRollUp(this)',
        'appendToElement': li
    });

    this.createElementFromArgs('span', {
        'class': 'dom-inspector-panel-content-node dom-inspector-panel-content-node-color',
        'innerHTML': '&lt' + Node.nodeName.toLowerCase() + '&gt',
        'appendToElement': li,
        'data-xpath': xPath
    });

    var ulHTMLElement = this.createElementFromArgs('ul', {
        'class': 'dom-inspector-panel-content-shown',
        'appendToElement': li

    });

    this.createElementFromArgs('span', {
        'class': 'dom-inspector-panel-content-node dom-inspector-panel-content-node-color',
        'innerHTML': '&lt/' + Node.nodeName.toLowerCase() + '&gt',
        'appendToElement': li,
        'data-xpath': xPath
    });

    return ulHTMLElement;

}

/**
 * Method creates HTML node and add it into HTML DOM Panel element.
 * @param Node - actual process node.
 * @param HTMLElement - parent HTML element.
 * @param InHTMLElement - actual HTML element.
 * @param XPath - accumulated XPath.
 */
DomInspector.prototype.addNode = function (Node, HTMLElement, InHTMLElement, XPath) {
    //console.log(Node.nodeName + "\t" + XPath);
    switch (Node.nodeType) {
        // NODE_ELEMENT
        case 1:
            //console.log(Level + ': Node Name: ' + Node.nodeName + ' Node Value: ' + Node.nodeValue + ' Node Type: ' + 'NODE_ELEMENT');
            var ulHTMLElement = this.createDomInspectorElement(Node, HTMLElement);

            if (Node.nodeName.toLowerCase() === "body") {
                InHTMLElement = true;
                Node.setAttribute('data-dom-insp-xpath', XPath);

            }

            //console.log(Node.nodeName + "\t" + XPath + '/span[2]');

            if (InHTMLElement == true && Node.nodeName.toLowerCase() !== "body") {
                this.registerEventListenerOnHTMLNode(Node);
                Node.setAttribute('data-dom-insp-xpath', XPath);
            }

            this.addNodes(Node.childNodes, ulHTMLElement, InHTMLElement, XPath + "/ul/li");
            break;
        // NODE_DOCUMENT
        case 9:
            // First create li element.
            var li = this.createElementFromArgs('li', {
                'class': 'dom-inspector-panel-content-li',
                'appendToElement': HTMLElement
            });

            this.createElementFromArgs('span', {
                'class': 'dom-inspector-panel-content-trigger dom-unpacked',
                'appendToElement': li
            });

            this.createElementFromArgs('span', {
                'class': 'dom-inspector-panel-content-node dom-inspector-panel-content-node-color',
                'innerHTML': Node.nodeName,
                'appendToElement': li
            });

            var ulHTMLElement = this.createElementFromArgs('ul', {
                'class': 'dom-inspector-panel-content-opened',
                'appendToElement': li
            });

            //console.log(Node.nodeName + "\t" + XPath);

            this.addNodes(Node.childNodes, ulHTMLElement, InHTMLElement, XPath + 'ul/li');

            break;
        // NODE_TEXT
        case 3:
//            console.log('Node Name: ' + Node.nodeName + ' Node Value: ' + Node.nodeValue + ' Node Type: ' + 'NODE_TEXT:' + Node.data);

            var li = this.createElementFromArgs('li', {
                'appendToElement': HTMLElement
            });

            var span = this.createElementFromArgs('span', {
                'class': 'dom-inspector-panel-content-text',
                'innerHTML': Node.data,
                'appendToElement': li
            });

            break;
    }
}

/**
 * Method inserts DOM panel into HTML page.
 */
DomInspector.prototype.visualize = function () {
    document.body.appendChild(this.domInspectorWrapper);
}

/**
 * Method returns content of
 * @returns {*}
 */
DomInspector.prototype.getContentHTMLElement = function () {
    return this.domInspectorWrapper.getElementsByTagName('div')[2];
}

/**
 * Method registers all roll up event listeners.
 */
DomInspector.prototype.registerRollUpEventListeners = function () {
    var triggerNodeList = this.domInspectorWrapper.getElementsByClassName('dom-inspector-panel-content-trigger');
    for (var i = 0; i < triggerNodeList.length; i++) {
        triggerNodeList[i].addEventListener('click', domInspectorRollUp);
    }
}

/**
 * Method return HTML element of DOM ID input field.
 * @returns {HTMLElement}
 */
DomInspector.prototype.getIDInputElement = function () {
    return document.getElementById(this.inputIDPanelID);
}

/**
 * Method return HTML element of DOM Class input field.
 * @returns {HTMLElement}
 */
DomInspector.prototype.getClassInputElement = function () {
    return document.getElementById(this.inputClassPanelID);
}

/**
 * Method registers all on lose focus event listeners.
 */
DomInspector.prototype.registerOnLoseFocusEventListeners = function () {

    var idInput = this.getIDInputElement();

    idInput.onblur = domInspectorOnInputLoseFocus;

    var classInput = this.getClassInputElement();

    classInput.onblur = domInspectorOnInputLoseFocus;

}

/**
 * Method registers click event listener on given node.
 * @param Node - on which node event listener has to be registered.
 */
DomInspector.prototype.registerEventListenerOnHTMLNode = function (Node) {

    // console.log("Node: " + Node.nodeName + " is being registered!");
    Node.addEventListener('click', domInspectorOnHTMLElementClick);
}

/**
 * Method registers given events on all dom inspector node elements.
 * @param EventMap - the map of events that has to be registered.
 */
DomInspector.prototype.registerEventListenersOnInspectorNodes = function (EventMap) {
    var triggerNodeList = this.domInspectorWrapper.getElementsByClassName('dom-inspector-panel-content-node');
    for (var i = 0; i < triggerNodeList.length; i++) {


        for (var event in EventMap) {

            //console.log("Registering " + event + " event on " + triggerNodeList[i].nodeName);
            triggerNodeList[i].addEventListener(event, EventMap[event]);
        }
    }
}


/**
 * Function triggered whenever MouseOver Event occurs on dom-inspector-panel-content-node element.
 * @param event
 */
function onMouseOver(event) {
    event = event || window.event;

    var target = event.target || event.srcElement;

    if (target.nodeType == 3) target = target.parentNode; // defeat Safari bug

    //console.log("onMouseOver");


    if (target.innerHTML.indexOf('/') == -1) {
        removeClass(target, 'dom-inspector-panel-content-node-color');
        addClass(target, 'dom-inspector-panel-content-node-color-onmouseover');

        removeClass(target.nextSibling.nextSibling, 'dom-inspector-panel-content-node-color');
        addClass(target.nextSibling.nextSibling, 'dom-inspector-panel-content-node-color-onmouseover');
    } else {
        removeClass(target, 'dom-inspector-panel-content-node-color');
        addClass(target, 'dom-inspector-panel-content-node-color-onmouseover');

        removeClass(target.previousSibling.previousSibling, 'dom-inspector-panel-content-node-color');
        addClass(target.previousSibling.previousSibling, 'dom-inspector-panel-content-node-color-onmouseover');
    }


    if (target.innerHTML !== "#document") {
        addClass(getElementByXPath(target.getAttribute('data-xpath')), 'dom-inspector-marker-border');
    }
}


/**
 * Function triggered whenever MouseOver Event occurs on dom-inspector-panel-content-node element.
 * @param event
 */
function domInspectorOnMouseOut(event) {
    event = event || window.event;

    var target = event.target || event.srcElement;

    if (target.nodeType == 3) target = target.parentNode; // defeat Safari bug

    //console.log("domInspectorOnMouseOut");

    if (target.innerHTML.indexOf('/') == -1) {
        addClass(target, 'dom-inspector-panel-content-node-color');
        removeClass(target, 'dom-inspector-panel-content-node-color-onmouseover');

        addClass(target.nextSibling.nextSibling, 'dom-inspector-panel-content-node-color');
        removeClass(target.nextSibling.nextSibling, 'dom-inspector-panel-content-node-color-onmouseover');
    } else {
        addClass(target, 'dom-inspector-panel-content-node-color');
        removeClass(target, 'dom-inspector-panel-content-node-color-onmouseover');

        addClass(target.previousSibling.previousSibling, 'dom-inspector-panel-content-node-color');
        removeClass(target.previousSibling.previousSibling, 'dom-inspector-panel-content-node-color-onmouseover');
    }

    removeClass(getElementByXPath(target.getAttribute('data-xpath')), 'dom-inspector-marker-border');
}

/**
 * Function triggered whenever Lose focut Event occurs on dom-inspector-panel-content-input element.
 * @param event
 */
function domInspectorOnInputLoseFocus(event) {
    event = event || window.event;

    var target = event.target || event.srcElement;

    var selectedElement = getElementByXPath(domInspector.getSelectedElement().getAttribute('data-xpath'));

    if (target.getAttribute('id').indexOf(domInspector.inputIDPanelID) == -1) {
            selectedElement.setAttribute('class', domInspector.getClassPanelText());
    } else {
            selectedElement.setAttribute('id', domInspector.getIDPanelText());
    }
}

/**
 * Function add class attribute into Node element.
 * @param Node - [Node]
 * @param Class - [String]
 */
function addClass(Node, Class) {

    // If node is null, then add.
    if (Node == null) {
        return;
    }

    if (Node.className) {
        var classes = Node.className.split(' ');

        // If its empty
        if (classes.length == 0) {
            Node.className += Class;
            return;
        }

        // Add class only if it does not exists.
        if (!contains(classes, Class)) {
            Node.className += " " + Class;
        }
    } else {
        Node.setAttribute('class', Class);
    }
}

/**
 * Function checks if Array contains passed in object.
 * @param Array
 * @param Object
 * @returns {boolean}
 */
function contains(Array, Object) {

    for (var i = 0; i < Array.length; i++) {
        if (Array[i] === Object) {
            return true;
        }
    }
    return false;
}

/**
 * Class removes Class name from Node element.
 * @param Node - [Node] Node on which removing is performed.
 * @param Class - [string] name of the class to be removed.
 */
function removeClass(Node, Class) {
    // If className attribute exists and there is something to remove
    if (Node && Node.className && Node.className.indexOf(Class) >= 0) {
        // Create regular expression
        var pattern = new RegExp('\\s*' + Class + '\\s*');

        // Replace regular expression with ' '
        Node.className = Node.className.replace(pattern, ' ').trim();
    }
}

/**
 * Function evaluates XPath and return chosen element if exists. Otherwise it returns null.
 * @param XPath - xpath string.
 * @returns {Node}
 */
function getElementByXPath(XPath) {
    // If browser is IE

    if (XPath == null)
        return document;

    // Get array of elements of XPath.
    var xPathElements = XPath.split("/");

    // Shift first empty elments, if any.
    if (xPathElements[0] === "") {
        xPathElements.shift();
        xPathElements.shift();
    }

    //console.log(XPath.toString() + "\t" + xPathElements.toString());

    var xPathElement = xPathElements.shift().toLowerCase();

    // Evaluate XPath element array. Start searching from specific element.
    if (xPathElement === "body") {
        // If BODY element is picket, return body
        if (xPathElements.length === 0) {
            return document.getElementsByTagName("body")[0];
        }
        return evaluateXPathIE(xPathElements, document.getElementsByTagName("body")[0].childNodes);
    } else if (xPathElement === "html") {
        if (xPathElements.length === 0) {
            return document.getElementsByTagName("HTML")[0];
        }
        return evaluateXPathIE(xPathElements, document.getElementsByTagName("HTML")[0].childNodes);
    } else {
        return evaluateXPathIE(xPathElements, document.getElementById(xPathElement.split("'")[1]).childNodes)
    }
    //return document.evaluate(XPath, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
    /*} else {
     return document.evaluate(XPath, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
     }*/
}

function removeSubstring(String) {
    return String.split(';')[1].split('&')[0];
}

/**
 * Initialize script.
 */
function domInspectorPanelInit() {
    // Create Dom Inspector object
    domInspector = new DomInspector('dom-inspector-info-panel', 'dom-inspector-title');

    // Initialize elements.
    domInspector.initialize();

    var ulHTMLElement = domInspector.createElementFromArgs('ul', {
        'id': 'dom-inspector-panel-content-ul'
    });

    domInspector.getContentHTMLElement().appendChild(ulHTMLElement);

    // Full the dom content panel.
    domInspector.addNode(document, ulHTMLElement, false, "//ul[@id='dom-inspector-panel-content-ul']/li/");

    // Show dom panel.
    domInspector.visualize();

    // Add Event Listners for roll up and roll down.
    domInspector.registerRollUpEventListeners();

    // Add Event Listners for roll up and roll down.
    domInspector.registerEventListenersOnInspectorNodes({
        'mouseout': domInspectorOnMouseOut,
        'mouseover': onMouseOver,
        'click': domInspectorOnMouseUp
    });

    domInspector.registerOnLoseFocusEventListeners();

}

/**
 * Helper function which erases class from all elements.
 * @param className
 */
function removeClassesFromAllElement(className) {
    // convert the result to an Array object
    var els = Array.prototype.slice.call(
        document.getElementsByClassName(className)
    );
    for (var i = 0, l = els.length; i < l; i++) {
        var el = els[i];
        el.className = el.className.replace(
            new RegExp('(^|\\s+)' + className + '(\\s+|$)', 'g'),
            '$1'
        );
    }
}

/**
 * Taken from <http://stackoverflow.com/questions/2631820/im-storing-click-coordinates-in-my-db-and-then-reloading-them-later-and-showing/2631931#2631931>
 * @param element
 * @returns {*} xPath of element.
 */
function getPathTo(element) {
//    if (element.id !== '')
//        return 'id("' + element.id + '")';

    if (element.nodeName === 'HTML') {
        return 'HTML';
    }

    if (element === document.body)
        return '//' + element.tagName;

    var ix = 0;
    var siblings = element.parentNode.childNodes;
    for (var i = 0; i < siblings.length; i++) {
        var sibling = siblings[i];
        if (sibling === element)
            return getPathTo(element.parentNode) + '/' + element.tagName + '[' + (ix + 1) + ']';
        if (sibling.nodeType === 1 && sibling.tagName === element.tagName)
            ix++;
    }
}

/**
 * Method is triggered whenever on mouse over event occurs.
 * @param event
 */
function domInspectorRollUp(event) {

    event = event || window.event;

    var target = event.target || event.srcElement;
    if (target.nodeType == 3) target = target.parentNode; // defeat Safari bug

    var classAttr = target.getAttribute('class');

    //console.log(classAttr);
    target.removeAttribute('class');

    //console.log(classAttr.indexOf('dom-packed'));

    if (classAttr.indexOf('dom-packed') == -1) {
        target.setAttribute('class', 'dom-inspector-panel-content-trigger dom-packed');
        target.nextSibling.nextSibling.setAttribute('class', 'dom-inspector-panel-content-hidden');
    } else {
        target.setAttribute('class', 'dom-inspector-panel-content-trigger dom-unpacked');
        target.nextSibling.nextSibling.setAttribute('class', 'dom-inspector-panel-content-shown');
    }
}

/**
 * Method is called whenever on HTML element is clicked.
 * @param event
 */
function domInspectorOnHTMLElementClick(event) {

//    console.log('domInspectorOnHTMLElementClick Event!');

    event = event || window.event;

    var target = event.target || event.srcElement;

    if (target.nodeType == 3) target = target.parentNode; // defeat Safari bug

    //alert('HTML Element clicked on ');

    //console.log(event + ' ' + target.nodeName + ' ' + target.getAttribute('data-dom-insp-xpath') + '/span[2]');

    removeClassesFromAllElement('dom-inspector-panel-content-node-selected');

    var domInspectorCorrespondingElement1 = getElementByXPath(target.getAttribute('data-dom-insp-xpath') + '/span[2]');

    var domInspectorCorrespondingElement2 = getElementByXPath(target.getAttribute('data-dom-insp-xpath') + '/span[3]');

    addClass(domInspectorCorrespondingElement1, 'dom-inspector-panel-content-node-selected');

    addClass(domInspectorCorrespondingElement2, 'dom-inspector-panel-content-node-selected');

    domInspector.refreshIdAndClass();

}

/**
 * Function is triggered whenever on mouse up event occurs.
 * @param event
 */
function domInspectorOnMouseUp(event) {

    //console.log('domInspectorOnMouseUp Event!');
    event = event || window.event;


    var target = event.target || event.srcElement;
    if (target.nodeType == 3) target = target.parentNode; // defeat Safari bug

    // Remove all classes.
    removeClassesFromAllElement('dom-inspector-panel-content-node-selected');

    if (target.innerHTML.indexOf('/') == -1) {
        addClass(target, 'dom-inspector-panel-content-node-selected');
        addClass(target.nextSibling.nextSibling, 'dom-inspector-panel-content-node-selected');
    } else {
        addClass(target, 'dom-inspector-panel-content-node-selected');
        addClass(target.previousSibling.previousSibling, 'dom-inspector-panel-content-node-selected');
    }

    domInspector.refreshIdAndClass();
}

/**
 * Function returns Input ID panel element.
 * @returns {HTMLElement}
 */
DomInspector.prototype.getDomInspectorIdPanel = function () {
    return document.getElementById(this.inputIDPanelID);
}

/**
 * Function return Input CLASS panel element.
 * @returns {HTMLElement}
 */
DomInspector.prototype.getDomInspectorClassPanel = function () {
    return document.getElementById(this.inputClassPanelID);
}

/**
 * Function erases text in Input ID and Class panel.
 */
DomInspector.prototype.eraseInspectorInputPanels = function () {
    this.getDomInspectorIdPanel().value = "";
    this.getDomInspectorClassPanel().value = "";
}

/**
 * Function refreshes ID and Class input panel.
 */
DomInspector.prototype.refreshIdAndClass = function () {

    this.eraseInspectorInputPanels();

    var element = getElementByXPath(this.getSelectedElement().getAttribute('data-xpath'));

    if (element) {
        var idAttribute = element.getAttribute('id');
        var classAttribute = element.getAttribute('class');

        var inputIDElement = this.getDomInspectorIdPanel();

        if (idAttribute) {
            inputIDElement.value = idAttribute;
        }

        var inputClassElement = this.getDomInspectorClassPanel();

        if (classAttribute) {


            inputClassElement.value = classAttribute.replace('dom-inspector-marker-border', '');
        }

    }
}

function evaluateXPathIE(XPathElements, ChildNodes) {

    var xPathElementName = XPathElements[0].split("[")[0].toLowerCase();

    // Shift first one.
    var xPathElement = XPathElements.shift();

    // Does actual xpath element contains number ?
    var xPathElementHasNumber = (xPathElement.indexOf("[") == -1) ? false : true;

    var xPathElementNumber;

    // If element contains number, then parse it.
    if (xPathElementHasNumber) {
        xPathElementNumber = parseInt(xPathElement.slice(xPathElement.indexOf("[") + 1, xPathElement.indexOf("]")));
    }

    var childNodesCounter = 0;

    // Traverse child nodes.
    for (var i = 0; i < ChildNodes.length; i++) {

        var child = ChildNodes[i];

        // Is child equal to searched element ?
        if (child.nodeName.toLowerCase() === xPathElementName) {
            // Increment counter
            childNodesCounter++;

            if (xPathElementHasNumber) {
                if (childNodesCounter == xPathElementNumber) {
                    if (XPathElements.length == 0) {
                        return child;
                    } else {
                        return evaluateXPathIE(XPathElements, child.childNodes);
                    }
                }
                // If has not number, return first found.
            } else {
                if (XPathElements.length == 0) {
                    return child;
                }
                return evaluateXPathIE(XPathElements, child.childNodes);
            }
        }
    }
}


/**
 * Function returns current selected element in DomInspector.
 * @returns {*}
 */
DomInspector.prototype.getSelectedElement = function () {

    var array = document.getElementsByClassName('dom-inspector-panel-content-node-selected');

    if (array) {
        return array[0];
    } else {
        return null;
    }
}

function getSelectedElement() {
    domInspector.getSelectedElement();
}